﻿Если ПараметрыОбъекта <> Неопределено Тогда

	ЗначениеСвойстваПокупатель = ПараметрыОбъекта["ЭтоПокупатель"];
	ЗначениеСвойстваПоставщик = ПараметрыОбъекта["ЭтоПоставщик"];
	
	ЗначениеСвойстваПостоплата = ПараметрыОбъекта["ЭтоПостоплата"];
	ЗначениеСвойстваПостоплатаНачДата = ПараметрыОбъекта["ПостоплатаНачДата"];
	ЗначениеСвойстваПостоплатаКонДата = ПараметрыОбъекта["ПостоплатаКонДата"];
	ЗначениеСвойстваПостоплатаСрок = ПараметрыОбъекта["ПостоплатаСрок"];
	ЗначениеСвойстваПостоплатаСумма = ПараметрыОбъекта["ПостоплатаСумма"];
	ЗначениеСвойстваИП = ПараметрыОбъекта["ЭтоИП"];
	ЗначениеСвойстваПоставкаПодРеализацию = ПараметрыОбъекта["ПоставкаПодРеализацию"];
	
	Если НЕ Объект.ЭтоГруппа Тогда
		Объект.Покупатель = ЗначениеСвойстваПокупатель;
		Объект.Поставщик = ЗначениеСвойстваПоставщик;
		
		Если ЗначениеСвойстваПостоплата = Истина Тогда
			Если ЗначениеЗаполнено(ЗначениеСвойстваПостоплатаНачДата) Тогда
				Объект.WT_ДатаНачалаДействияДоговора = ЗначениеСвойстваПостоплатаНачДата;
				Объект.WT_ДатаБлокированияДоговора = ЗначениеСвойстваПостоплатаКонДата;
				Объект.WT_ОтсрочкаПлатежаСрокОплаты = ЗначениеСвойстваПостоплатаСрок;
				Объект.WT_ОтсрочкаПлатежаСуммаЛимита = ЗначениеСвойстваПостоплатаСумма;
			КонецЕсли;
		КонецЕсли;	
		
		Запрос = Новый Запрос("Выбрать Первые 1
		|	WT_Объекты.GUID
		|ИЗ
		|	РегистрСведений.WT_Объекты Как WT_Объекты
		|ГДЕ
		|	WT_Объекты.УзелОбмена = &УзелОбмена
		|	И WT_Объекты.ТипОбъекта = &ТипОбъекта
		|	И WT_Объекты.Объект = &Объект");
		Запрос.УстановитьПараметр("УзелОбмена", ПланыОбмена.WT_Обмен_WTIS_1С.НайтиПоКоду("001"));
		Запрос.УстановитьПараметр("ТипОбъекта", "RefKontr");
		Запрос.УстановитьПараметр("Объект", Объект.Ссылка);
		Результат = Запрос.Выполнить();
		
		Если Результат.Пустой() Тогда
			ГуидВтис = ПараметрыОбъекта["ГуидВтис"];	
			Если ЗначениеЗаполнено(ГуидВтис) Тогда
				Объект.Записать();
				РегОбъектыВИ = РегистрыСведений.WT_Объекты.СоздатьМенеджерЗаписи();
				РегОбъектыВИ.УзелОбмена = ПланыОбмена.WT_Обмен_WTIS_1С.НайтиПоКоду("001");
				РегОбъектыВИ.ТипОбъекта = "RefKontr";
				РегОбъектыВИ.GUID = ПараметрыОбъекта["ГуидВтис"];
				РегОбъектыВИ.Объект = Объект.Ссылка;
				РегОбъектыВИ.Записать(Истина);
			КонецЕсли;	
		КонецЕсли;				
		
	КонецЕсли;	
	
КонецЕсли;


Если НЕ Объект.ЭтоГруппа Тогда

	Если Не ЗначениеЗаполнено(Объект.ЮрФизЛицо) Тогда
		Если ЗначениеЗаполнено(Объект.ИНН) и Найти(Объект.ИНН,"00000") = 0 Тогда
			Объект.ЮрФизЛицо = Перечисления.ЮрФизЛицо.ЮрЛицо;
		Иначе
			Объект.ЮрФизЛицо = Перечисления.ЮрФизЛицо.ФизЛицо;
		КонецЕсли;	
	КонецЕсли;
	
	Если Объект.ЭтоНовый() Тогда
		Объект.Записать();
	КонецЕсли;
	
	СвойствоТипОплаты = ПланыВидовХарактеристик.СвойстваОбъектов.НайтиПоНаименованию("Тип оплаты",Истина);
	РегСвойства = РегистрыСведений.ЗначенияСвойствОбъектов.СоздатьМенеджерЗаписи();
	РегСвойства.Объект = Объект.Ссылка;
	РегСвойства.Свойство = СвойствоТипОплаты;
	Если ЗначениеСвойстваПостоплата = Истина Тогда
		РегСвойства.Значение = Справочники.ЗначенияСвойствОбъектов.НайтиПоНаименованию("Постоплата",Истина);
	Иначе
		РегСвойства.Значение = Справочники.ЗначенияСвойствОбъектов.НайтиПоНаименованию("Предоплата",Истина);
	КонецЕсли;	
	РегСвойства.Записать(Истина);
	
	Если ЗначениеЗаполнено(ЗначениеСвойстваПоставкаПодРеализацию) Тогда
		ПоставкаПодРеализацию = ПланыВидовХарактеристик.СвойстваОбъектов.НайтиПоНаименованию("Поставка под реализацию",Истина);
		РегСвойства = РегистрыСведений.ЗначенияСвойствОбъектов.СоздатьМенеджерЗаписи();
		РегСвойства.Объект = Объект.Ссылка;
		РегСвойства.Свойство = ПоставкаПодРеализацию;
		РегСвойства.Значение = ЗначениеСвойстваПоставкаПодРеализацию; 
		РегСвойства.Записать(Истина);
	КонецЕсли;
	
	Если ЗначениеСвойстваИП = Истина Тогда
		КатегорияИП = Справочники.КатегорииОбъектов.НайтиПоНаименованию("Индивидуальный предприниматель",Истина);
		ЗаписьИП = РегистрыСведений.КатегорииОбъектов.СоздатьМенеджерЗаписи();
		ЗаписьИП.Объект    = Объект.Ссылка;
		ЗаписьИП.Категория = КатегорияИП;
		ЗаписьИП.Записать(Истина);
	КонецЕсли;
	
КонецЕсли;
