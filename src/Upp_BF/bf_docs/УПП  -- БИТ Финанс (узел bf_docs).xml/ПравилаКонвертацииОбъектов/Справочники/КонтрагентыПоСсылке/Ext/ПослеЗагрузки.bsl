﻿Если НЕ Объект.ЭтоГруппа Тогда

	//Если ЗначениеЗаполнено(Объект.Ссылка.ВИ_ГуидВтис) Тогда
	//	
	//	Если Объект.ЭтоНовый() Тогда
	//		Объект.Записать();
	//	КонецЕсли;	
	//	
	//	РегОбъектыВИ = РегистрыСведений.ВИ_Объекты.СоздатьМенеджерЗаписи();
	//	РегОбъектыВИ.УзелОбмена = ПланыОбмена.ВИ_Обмен_ВТИС_1С.НайтиПоКоду("001");
	//	РегОбъектыВИ.ТипОбъекта = "RefKontr";
	//	РегОбъектыВИ.GUID = Объект.Ссылка.ВИ_ГуидВтис;
	//	РегОбъектыВИ.Ссылка = Объект.Ссылка;
	//	РегОбъектыВИ.Записать(Истина);
	//	
	//КонецЕсли;
	//
	
	ГуидВтис = ПараметрыОбъекта["ГуидВтис"];
	
	Если ЗначениеЗаполнено(ГуидВтис) Тогда
		
		Если Объект.ЭтоНовый() Тогда
			Объект.обменданными.Загрузка = истина;
			Объект.Записать();
		КонецЕсли;	
		
		ГуидЗадвоен = Ложь;
		Выполнить(Алгоритмы.ПроверкаЗадвоенияГуидаВтис);
		
		Если НЕ ГуидЗадвоен Тогда
		
			РегОбъектыВИ = РегистрыСведений.ВИ_Объекты.СоздатьМенеджерЗаписи();
			РегОбъектыВИ.УзелОбмена = ПланыОбмена.ВИ_Обмен_ВТИС_1С.НайтиПоКоду("001");
			РегОбъектыВИ.ТипОбъекта = "RefKontr";
			РегОбъектыВИ.GUID = ПараметрыОбъекта["ГуидВтис"];
			РегОбъектыВИ.Ссылка = Объект.Ссылка;
			РегОбъектыВИ.Записать(Истина);
		
		КонецЕсли;
		
		
	КонецЕсли;


	Если ТипЗнч(Объект.ОсновнойБанковскийСчет.Владелец) = Тип("СправочникСсылка.ФизическиеЛица") Тогда
		Объект.ОсновнойБанковскийСчет = Справочники.БанковскиеСчета.ПустаяСсылка();	
	КонецЕсли;	
	
	Выполнить(Алгоритмы.ЗаполнитьСвойстваКонтрагента);
	
	Если НЕ Объект.ВИ_Активен Тогда
		Объект.ВИ_Активен = Истина;	
	КонецЕсли;	
	
	Если Объект.Родитель = Справочники.Контрагенты.НайтиПоНаименованию("Подотчетные лица",Истина) Тогда
		Выполнить(Алгоритмы.ОбработатьБанковскийСчетФизлица);
	КонецЕсли;	
	
	Если Не ЗначениеЗаполнено(Объект.ЮридическоеФизическоеЛицо) Тогда
		Если ЗначениеЗаполнено(Объект.ИНН) и Найти(Объект.ИНН,"00000") = 0 Тогда
			Объект.ЮридическоеФизическоеЛицо = Перечисления.ЮридическоеФизическоеЛицо.ЮридическоеЛицо;
		Иначе
			Объект.ЮридическоеФизическоеЛицо = Перечисления.ЮридическоеФизическоеЛицо.ФизическоеЛицо;
		КонецЕсли;	
	КонецЕсли;
	
	//Обработчике загрузки УниверсальныйОбменДаннымиXML есть фича, при которой ПометкаУдаления являющаяся полем поиска сбрасывается в ЛОЖЬ.
	ПометкаУдаления = ПараметрыОбъекта["ПометкаУдаления"];
	
	
КонецЕсли;
