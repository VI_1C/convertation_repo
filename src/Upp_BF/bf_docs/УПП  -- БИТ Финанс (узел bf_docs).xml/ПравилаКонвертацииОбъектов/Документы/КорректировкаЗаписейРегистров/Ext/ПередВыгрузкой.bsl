﻿Товар18 = Справочники.Номенклатура.НайтиПоНаименованию("Товар_НДС18",истина);
Товар10 = Справочники.Номенклатура.НайтиПоНаименованию("Товар_НДС10",истина);
Товар0 = Справочники.Номенклатура.НайтиПоНаименованию("Товар_НДС0",истина);
	
Запрос = Новый Запрос("ВЫБРАТЬ
                      |	Корректировка.Ссылка КАК Регистратор,
                      |	Корректировка.Номер,
                      |	Корректировка.Дата КАК Дата,
                      |	Корректировка.Комментарий,
                      |	Корректировка.Ответственный
                      |ИЗ
                      |	Документ.КорректировкаЗаписейРегистров КАК Корректировка
                      |
					  |ГДЕ
	                  |	Корректировка.Ссылка = &Ссылка
                      |УПОРЯДОЧИТЬ ПО
                      |	Дата");	
Запрос.УстановитьПараметр("Ссылка", Источник);
Выборка = Запрос.Выполнить().Выбрать();

Если Выборка.Количество() = 0 Тогда
	Отказ = Истина;
КонецЕсли;	

Пока Выборка.Следующий() цикл
	
	
	Проводки = РегистрыБухгалтерии.Хозрасчетный.СоздатьНаборЗаписей();
	Проводки.Отбор.Регистратор.Значение = Выборка.Регистратор;
	Проводки.Прочитать();

    ТаблицаДвижений = Новый ТаблицаЗначений;
    ТаблицаДвижений.Колонки.Добавить("Активность");
    ТаблицаДвижений.Колонки.Добавить("Период");
    ТаблицаДвижений.Колонки.Добавить("Организация");
    ТаблицаДвижений.Колонки.Добавить("Содержание");		
	ТаблицаДвижений.Колонки.Добавить("СчетДт");
	ТаблицаДвижений.Колонки.Добавить("СчетКт");
	ТаблицаДвижений.Колонки.Добавить("СубконтоДт");
    ТаблицаДвижений.Колонки.Добавить("СубконтоКт");
	ТаблицаДвижений.Колонки.Добавить("ПодразделениеДт");
    ТаблицаДвижений.Колонки.Добавить("ПодразделениеКт");
	ТаблицаДвижений.Колонки.Добавить("ВалютаДт");
	ТаблицаДвижений.Колонки.Добавить("ВалютаКт");
	ТаблицаДвижений.Колонки.Добавить("ВалютнаяСуммаДт");
	ТаблицаДвижений.Колонки.Добавить("ВалютнаяСуммаКт");
	ТаблицаДвижений.Колонки.Добавить("КоличествоДт");
	ТаблицаДвижений.Колонки.Добавить("КоличествоКт");
	ТаблицаДвижений.Колонки.Добавить("Сумма");
	//ТаблицаДвижений.Колонки.Добавить("СуммаНУДт");
	//ТаблицаДвижений.Колонки.Добавить("СуммаНУКт");	
	
	Если Проводки.Количество() = 0 Тогда
		Отказ = Истина;
	КонецЕсли;

	Для Каждого Движение из Проводки цикл

		СтрокаТаблицаДвижений = ТаблицаДвижений.Добавить();
		ЗаполнитьЗначенияСвойств(СтрокаТаблицаДвижений,Движение);
		
		СубконтоДт = Новый Соответствие;
		Для каждого Субконто Из Движение.СубконтоДт Цикл
			Если ТипЗнч(Субконто.Значение) = Тип("СправочникСсылка.ПодразделенияОрганизаций") тогда
				СтрокаТаблицаДвижений.ПодразделениеДт = Субконто.Значение;
            	Продолжить;
			КонецЕсли;
				СубконтоДт.Вставить(Субконто.Ключ, Субконто.Значение);
		КонецЦикла;	
			
		СтрокаТаблицаДвижений.СубконтоДт = СубконтоДт;
		
		СубконтоКт = Новый Соответствие;
		
		Если Движение.СчетКт = ПланыСчетов.Хозрасчетный.ТоварыНаСкладах тогда
			
			Для каждого Субконто Из Движение.СубконтоКт Цикл
				Если ТипЗнч(Субконто.Значение) = Тип("СправочникСсылка.Номенклатура") тогда
					Если Субконто.Значение.СтавкаНДС = Перечисления.СтавкиНДС.НДС18 тогда
						СубконтоКт.Вставить(Субконто.Ключ, Товар18);
					ИначеЕсли Субконто.Значение.СтавкаНДС = Перечисления.СтавкиНДС.НДС10 тогда
						СубконтоКт.Вставить(Субконто.Ключ, Товар10);
					ИначеЕсли Субконто.Значение.СтавкаНДС = Перечисления.СтавкиНДС.НДС0 или Субконто.Значение.СтавкаНДС = Перечисления.СтавкиНДС.БезНДС тогда	
						СубконтоКт.Вставить(Субконто.Ключ, Товар0);						
					КонецЕсли;	
				КонецЕсли;	
				
				Если ТипЗнч(Субконто.Значение) = Тип("СправочникСсылка.Склады") тогда
					СубконтоКт.Вставить(Субконто.Ключ, Справочники.Склады.НайтиПоКоду("000000794")); /// основной склад
				КонецЕсли;					
				
				
			КонецЦикла;	
			
		Иначе	
			

			Для каждого Субконто Из Движение.СубконтоКт Цикл
				Если ТипЗнч(Субконто.Значение) = Тип("СправочникСсылка.ПодразделенияОрганизаций") тогда
					СтрокаТаблицаДвижений.ПодразделениеКт = Субконто.Значение;
	            	Продолжить;
				КонецЕсли;
					СубконтоКт.Вставить(Субконто.Ключ, Субконто.Значение);
			КонецЦикла;	
			
		КонецЕсли;
				
		СтрокаТаблицаДвижений.СубконтоКт = СубконтоКт;

		
		Если СтрокаТаблицаДвижений.СчетДт = ПланыСчетов.Хозрасчетный.ИздержкиОбращенияНеОблагаемыеЕНВД Тогда
			СтрокаТаблицаДвижений.СчетДт = ПланыСчетов.Хозрасчетный.ОбщехозяйственныеРасходы;
		КонецЕсли;	
		
		Если СтрокаТаблицаДвижений.СчетКт = ПланыСчетов.Хозрасчетный.ИздержкиОбращенияНеОблагаемыеЕНВД Тогда
			СтрокаТаблицаДвижений.СчетКт = ПланыСчетов.Хозрасчетный.ОбщехозяйственныеРасходы;
		КонецЕсли;	
		
		
	КонецЦикла;

	
	Если ТаблицаДвижений.Количество() > 0 тогда
		
		ИсточникСтруктура = Новый Структура("Номер, Дата, Организация, СуммаОперации, СпособЗаполнения, Комментарий, Ответственный");
		
		Номер = Выборка.Регистратор.номер;
		Пока Лев(Номер, 1)="0" Цикл
			Номер = Сред(Номер, 2);
		КонецЦикла;
		
		ИсточникСтруктура.Номер = "КЗР-"+Номер;
		ИсточникСтруктура.Дата = Выборка.Регистратор.Дата;
		ИсточникСтруктура.Организация = Параметры.Организация;
		ИсточникСтруктура.Комментарий = Выборка.Комментарий;
		//Источник.Содержание = СокрЛП(Выборка.Содержание);
		ИсточникСтруктура.СуммаОперации = ТаблицаДвижений.Итог("Сумма");
		ИсточникСтруктура.СпособЗаполнения = "Вручную";
		ИсточникСтруктура.Ответственный = Выборка.Ответственный;
		
		//ИсходящиеДанные = Новый Структура("Хозрасчетный", ТаблицаДвижений);
		ВходящиеДанные = Новый Структура("Хозрасчетный,Гуид", ТаблицаДвижений,Выборка.Регистратор.УникальныйИдентификатор());		
		ВходящиеДанные.Вставить("ИсточникСтруктура", ИсточникСтруктура);
		//ВыгрузитьПоПравилу(Источник,, ИсходящиеДанные,, "КорректировкаЗаписейРегистров");

	Иначе
		Отказ = истина;
	КонецЕсли;
	
КонецЦикла;
