﻿ПроектСсылка = Справочники.Проекты.НайтиПоНаименованию(ПараметрыОбъекта["Проект"],Истина);

НачатьТранзакцию();

	Попытка

		Если ПроектСсылка.Пустая() Тогда
			НовыйПроект = Справочники.Проекты.СоздатьЭлемент();
			НовыйПроект.Наименование = ПараметрыОбъекта["Проект"];
			НовыйПроект.Записать();
			
			ПроектСсылка = НовыйПроект.ссылка;
		КонецЕсли;	

		МенеджерЗаписиПроект = РегистрыСведений.бит_ДополнительныеАналитики.СоздатьМенеджерЗаписи();
		МенеджерЗаписиПроект.Объект = Объект.Ссылка;
		МенеджерЗаписиПроект.Аналитика = ПланыВидовХарактеристик.бит_ВидыДополнительныхАналитик.Проект;
		МенеджерЗаписиПроект.ЗначениеАналитики = ПроектСсылка;
		МенеджерЗаписиПроект.Записать();
		
		МенеджерЗаписиПроектПараметры = РегистрыСведений.бит_ДополнительныеПараметрыОбъектов.СоздатьМенеджерЗаписи();
		МенеджерЗаписиПроектПараметры.Объект = Объект.Ссылка;
		МенеджерЗаписиПроектПараметры.ЗапретитьПерезаполнениеАналитик = Истина;
		МенеджерЗаписиПроектПараметры.Записать();
		
		Исключение
			ОтменитьТранзакцию();
			Сообщить(ОписаниеОшибки());
			
	КонецПопытки;

ЗафиксироватьТранзакцию();
