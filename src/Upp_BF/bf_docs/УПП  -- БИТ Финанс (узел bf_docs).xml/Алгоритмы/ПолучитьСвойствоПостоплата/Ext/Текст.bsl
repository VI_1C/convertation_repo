﻿Запрос = Новый Запрос("ВЫБРАТЬ
|	ВЫБОР
|		КОГДА ЗначенияСвойствОбъектов.Значение.Наименование = ""Постоплата""
|			ТОГДА ИСТИНА
|		ИНАЧЕ ЛОЖЬ
|	КОНЕЦ КАК Постоплата
|ИЗ
|	РегистрСведений.ЗначенияСвойствОбъектов КАК ЗначенияСвойствОбъектов
|ГДЕ
|	ЗначенияСвойствОбъектов.Объект = &Объект
|	И ЗначенияСвойствОбъектов.Свойство = &Свойство");
Запрос.УстановитьПараметр("Объект",	Источник);	 
Запрос.УстановитьПараметр("Свойство", ПланыВидовХарактеристик.СвойстваОбъектов.НайтиПоНаименованию("Тип оплаты",истина));
Выборка = Запрос.Выполнить().Выбрать();

Если Выборка.Следующий() Тогда
	Значение = Выборка.Постоплата;
Иначе	
	Значение = Ложь;
КонецЕсли;
